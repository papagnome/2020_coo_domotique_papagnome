package domotique;
import static org.junit.Assert.*;
import org.junit.Test;
/**
 * classe de test permettant de tester la classe Telecommande
 */
public class TelecommandeTest {

		@Test
		/**
		 * test de ajouterLampe dans une Telecommande vide
		 */
		public void testAjouterLampeVide() {
			// methode testee
			Lampe l = new Lampe("l");
			Telecommande t = new Telecommande();
			t.ajouterLampe(l);
			
			//verification
			boolean res = t.lampes.get(0).isAllume();
			assertEquals("une nouvelle lampe devrait etre ajout�e a une telecommande vide", false, res);
		}
		
		@Test
		/**
		 * test de ajouterLampe dans une Telecommande avec deja 1 element
		 */
		public void testAjouterLampe() {
			// methode testee
			Lampe l = new Lampe("l");
			Lampe l2 = new Lampe("l2");
			Telecommande t = new Telecommande();
			t.ajouterLampe(l);
			t.ajouterLampe(l2);
			
			//verification
			boolean res = t.lampes.get(1).isAllume();
			assertEquals("une nouvelle lampe devrait etre ajout�e a la 2eme place", false, res);
		}
		
		@Test
		/**
		 * test de activerLampe en position 0
		 */
		public void testActiverLampe0() {
			// methode testee
			Lampe l = new Lampe("l");
			Telecommande t = new Telecommande();
			t.ajouterLampe(l);
			t.activerLampe(0);
			
			//verification
			boolean res = t.lampes.get(0).isAllume();
			assertEquals("la lampe l devrait etre allum�e", true, res);
		}
		
		@Test
		/**
		 * test de activerLampe en position 1
		 */
		public void testActiverLampe1() {
			// methode testee
			Lampe l = new Lampe("l");
			Lampe l2 = new Lampe("l2");
			Telecommande t = new Telecommande();
			t.ajouterLampe(l);
			t.ajouterLampe(l2);
			t.activerLampe(1);
			
			//verification
			boolean res = t.lampes.get(1).isAllume();
			assertEquals("la lampe l2 devrait etre allum�e", true, res);
		}
		
		@Test
		/**
		 * test de activerLampe d'une lampe inexistante
		 */
		public void testActiverLampeInexistante() {
			// methode testee
			Lampe l = new Lampe("l");
			Lampe l2 = new Lampe("l2");
			Lampe l3 = new Lampe("l3");
			Telecommande t = new Telecommande();
			t.ajouterLampe(l);
			t.ajouterLampe(l2);
			t.ajouterLampe(l3);
			t.activerLampe(2);
			
			//verification
			boolean res = t.lampes.get(1).isAllume();
			assertEquals("la lampe n'existe pas", true, res);
		}
}
