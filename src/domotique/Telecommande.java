package domotique;

import java.util.ArrayList;

public class Telecommande {
	ArrayList<Lampe> lampes;
	Hifi chaine;
	
	public Telecommande() {
		lampes = new ArrayList<Lampe>();
	}
	
	public void ajouterLampe(Lampe l) {
		this.lampes.add(l);
	}
	
	public void activerLampe(int indiceLampe) {
		this.lampes.get(indiceLampe).allumer();
	}
	
	public void desactiverLampe(int indiceLampe) {
		this.lampes.get(indiceLampe).eteindre();
	}
	
	public void activerTout() {
		for(int i=0; i<this.lampes.size();i++) {
			this.lampes.get(i).allumer();
		}
	}
	
	public void allumerHifi() {
		chaine.allumer();
	}
	
	public void eteindreHifi() {
		chaine.eteindre();
	}
}
